# 8 March 2025
## Year 3, month 3, patch 1

This patch will include new features, balance adjustments, bugfixes, and audiovisual improvements.

Download [Fraud Launcher 2](https://fraudsclub.com/files/Fraud%20Launcher%202.zip) for easier setup and maintenance! It now includes automatic setup and updates for CrownLink, a networking alternative for Cosmonarchy!

## Next patch...
Our goals for the next patch (8 March 2025) include:
- Reworked [Savant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/savant), [Siren](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/siren), and [Mind Tyrant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/mind-tyrant)
- Deployment of the [Tencendur](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/tencendur), [Xenolith](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/xenolith), and [Zobriolisk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/zobriolisk)
- More factional units for Golden Republic, Nydus Trinity, Starbound, and Imicren Tendril
- AI and stability improvements
- New terrain blends

## Bugfixes
- **Stability:**
	- Further reduced desync rate (a. DF)
	- Fixed a crash case related to rescuable units (r. & c. DF)
- **Gameplay:**
	- **Futurist Cleric**s no longer provide **Nanoshield** (r. jakulangski)
- **Performance:**
	- Movement speed calculations have been optimized, reducing performance cost of things like **Zoryusthaleth Swarming Omen** (c. DF)
- **Audiovisuals:**
	- Improved minimap legibility of Jet Black and Dim Grey player colors
	- Power fields now display when selecting allied power generators (c. DF)
	- Power fields now consistently show in witness mode and replay mode (c. DF)
	- Weapon tooltip for **Oscilliant** has been corrected (r. BladehollowRWL)

## Audiovisuals
- All non-factional units have received unique placeholder portraits (many c. jun3hong)
- New unit responses for:
	- **Ramesses**

## Terran
- [Savant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/savant): **Revised!**
	- **Breathing Battery:**
		- Now a proper transformation
		- Movement speed in Entropy mode 4.4 » 5
		- Now immobile in Siphon mode
	- **Discoid Railgun:**
		- Weapon range in Siphon mode 6 » 7
		- Armor penetration in Siphon mode 3 » 0
		- Weapon cooldown in Siphon mode 0.83 » 0.75
- [Madcap](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/madcap):
	- **Danaan Autogun:**
		- Weapon damage 1x6 » 1x7
- [Siren](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/siren): **Revised!**
	- Resource costs 75/75 » 75/100
	- HP 90 » 100
	- **Charybdal Rifle:**
		- Weapon factor 5 » 3
	- **NEW: Law of Attraction:**
		- Now pulls victims 2 range closer to the Siren
- [Goliath](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/goliath):
	- HP 220 200
	- **Twin Autocannons:**
		- Weapon damage 2x5 » 2x4
- [Blackjack](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/blackjack):
	- **Monogun:**
		- Weapon cooldown 0.54 » 0.5
- [Matador](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/matador): **Revised!**
	- No longer has Twinstrike Cannons
	- Now attacks 25% faster in Cinder Mode
- [Fulcrum](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/fulcrum):
	- Build time 45 » 50
- [Rotary](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/rotary):
	- Resource costs 200/75 » 175/75
	- Build time 35 » 30
- **NEW:** [Blackjack (Golden)](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=terran/blackjack-golden):
	- Golden Republic: Blackjack variant

## Protoss
- [Mind Tyrant](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/mind-tyrant) **Revised!**:
	- **Tyranny » Maledictus:**
		- Channels for 5 seconds before declaring the target their malefactor
		- While within 10 range of their tyrant, malefactors gain 33% movement speed, use the tyrant's energy as an overshield, and tyrannize hostiles on death
		- Tyrants can only have one malefactor at a time
	- **Maelstrom:**
		- Duration 4 seconds » 5 seconds
- [Accantor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/accantor):
	- Movement speed 1.63 » 1.8 (10% faster)
	- Removed variable movement speed steps
- [Architect](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/architect):
	- **Dispersive Salvo:**
		- Weapon damage 1x40 » 1x30
- [Vibrance](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/vibrance):
	- Movement speed 5 » 4.14 (17% slower)
	- **Sensory Overload:**
		- Weapon damage 1x15 » 1x10
- [Aquifer](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/aquifer) and [Matrix](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/matrix):
	- Now requires a tier 1 production structure
- [Crucible](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/crucible):
	- Now requires a tier 1 production structure
	- Now counts as a tier 2 production structure
- [Engram](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/engram):
	- Now requires a tier 2 production structure
- [Unitary Union](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/unitary-union):
	- Resource costs 400/300 » 400/350
- **NEW:** [Xenolith](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/xenolith):
	- Recalling power array; requires any tier 2
- **NEW:** [Dracadin (Order](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=protoss/dracadin-order):
	- Order of the Sacred Saber: Dracadin variant

## Zerg
- [Rilirokor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/rilirokor): **Revised!**
	- Can now be mutated from Larvae (dual-birth, no tech requirement)
	- Timed life now resets to full while on cagra
	- On attack, now gains +1 second of timed life
- [Hydralisk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/hydralisk):
	- Movement speed 6.52 » 6.398 (2% slower)
	- Removed variable movement speed steps
- [Kagralisk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/kagralisk):
	- Movement speed 4.496 » 4.797 (7% faster)
	- Removed variable movement speed steps
- [Gorgrokor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/gorgrokor):
	- Sight range 8 » 7
- [Ultrokor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/ultrokor):
	- Resource costs 250/150 » 300/150
	- HP 500 » 400
	- Armor 5 » 3
- [Zarcavrokor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/zarcavrokor):
	- Resource costs 175/150 » 200/125
	- HP 300 » 280
	- Armor 4 » 3
	- Sight range 10 » 9
- [Ultrav Cavern](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/ultrav-cavern):
	- Resource costs 1000/500 » 400/400
	- Build time 60 » 50
	- HP 1250 » 1200
	- Armor 8 » 7
	- Now requires Irol Iris, from Othstol Oviform
- **NEW:** [Zobriolisk](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/zobriolisk):
	- Versatile assault strain; morphed from Lakizilisk, requires Ultrav Cavern
- **NEW:** [Quazrokor](https://www.fraudsclub.com/cosmonarchy-bw/show.php?target=zerg/quazrokor):
	- Imicren Tendril: Quazilisk variant